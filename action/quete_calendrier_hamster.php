<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2013                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Gestion de l'action de quête des activités Hamster du calendrier
 *
 * @package SPIP\Hamster\Action
**/

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fournir une liste d'activités d'Hamster entre deux dates start et end
 * au format json
 * 
 * Utilisé pour l'affichage du calendrier des activités Hamster
 * 
 * @return void
 */
function action_quete_calendrier_hamster_dist(){
	$securiser_action = charger_fonction('securiser_action','inc');
	$securiser_action();

	$start = _request('start');
	$end   = _request('end');

	include_spip('action/quete_calendrier_prive');
	include_spip('inc/quete_calendrier');

	// recuperer la liste des evenements au format ics
	$start = date('Y-m-d H:i:s', $start);
	$end   = date('Y-m-d H:i:s', $end);

	$evt = array();
	$duree = quete_calendrier_interval_hamster($start, $end, $evt);
	$evt = convert_fullcalendar_quete_calendrier_hamster($duree);

	// format json
	include_spip('inc/json');
	echo json_encode($evt);
}



/**
 * Retourne la liste des activités (format ICS) présentes dans une période donnée
 *
 * @param string $debut
 *     Date de début
 * @param string $fin
 *     Date de fin
 * @param array $evenements
 *     Tableau des événements déjà présents qui sera complété par la fonction.
 *     Format : `$evenements[$amj][] = Tableau de description ICS`
 * @return array
 *     De la forme : `$evenements[date][id] = Tableau des données ICS`
**/
function quete_calendrier_interval_hamster($debut, $fin, &$evenements) {

	include_spip('inc/config');
	if (!$id_document = lire_config('hamster/document')
		OR !$fichier = sql_getfetsel('fichier', 'spip_documents', 'id_document=' . sql_quote($id_document)))
	{
		return $evenements;
	}

	include_spip('inc/documents');
	include_spip('inc/texte');
	$fichier = get_spip_doc($fichier);
	$xml = simplexml_load_file($fichier);

	foreach ($xml as $activity) {
		if ($activity['end_time'] >= $debut AND $activity['start_time'] <= $fin) {

			$amj = date_anneemoisjour($activity['start_time']);

			// description un peu plus élaborée
			$desc = trim($activity['description']);
			$desc .= "\n\n{{Durée :}} " . $activity['duration_minutes'] . " minutes";

			if ($activity['category']) {
				$desc .= "\n{{Catégorie :}} " . $activity['category'];
			}

			if ($tags = trim($activity['tags'])) {
				$desc .= "\n{{Mots-clés :}} " . $tags;
			}

			if (!isset($evenements[$amj])) {
				$evenements[$amj] = array();
			}
			// fullcalendar se débrouillera très bien de ça...
			$num = (crc32($activity['name'])%14)+1;
			$evenements[$amj][] = array(
				'CATEGORIES' => trim('calendrier-couleur' . $num . " " . $tags),
				'SUMMARY' => (string)$activity['name'],
				'URL' => '',
				'DTSTART' => date_ical($activity['start_time']),
				'DTEND'   => date_ical($activity['end_time']),
				'DESCRIPTION' => propre($desc),
			);
		}
	}

	return $evenements;
}



/**
 * Convertir une sortie événement de quete quete_calendrier_interval_hamster
 * dans le format attendu par fullcalendar
 *
 * @use convert_dateical()
 * 
 * @param array $messages
 *     Les événements / messages au format issu de la quete calendrier_interval_rv
 * @param array $evt
 *     Les événements au format fullcalendar déjà présents
 * @return array
 *     Les événements au format fullcalendar
**/
function convert_fullcalendar_quete_calendrier_hamster($messages, $evt = array()) {
	if (!$messages) return $evt;

	foreach($messages as $amj=>$l){
		foreach($l as $id=>$e){
			$url = str_replace('&amp;','&',$e['URL']);
			$evt[] = array(
				'id' => $id,
				'title' => $e['SUMMARY'],
				'allDay' => false,
				'start' => convert_dateical($e['DTSTART']), //Ymd\THis
				'end' => convert_dateical($e['DTEND']), // Ymd\THis
				'url' => $url,
				'className' => "calendrier-event ".$e['CATEGORIES'],
				'description' => $e['DESCRIPTION'],
			);
		}
	}
	return $evt;
}
