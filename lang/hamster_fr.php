<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'aucun_document' => 'Aucun document sélectionné',

	// B
	'bouton_hamster' => 'Visualiser le temps (Hamster)',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'configurer_hamster' => 'Configurer',

	// E
	'editer_le_document' => 'Éditer le document',

	// H
	'hamster_titre' => 'Visualisation de Hamster (compteur de temps)',

	// S
	'selectionner_document' => 'Sélectionner un document',
	'selectionner_document_explication' => 'Ce document doit être un export XML issu de Hamster',

	// T
	'titre_page_configurer_hamster' => 'Configurer la visualisation du rapport de Hamster',

	// V
	'visualiser_hamster' => 'Visualiser le calendrier',
);

?>
