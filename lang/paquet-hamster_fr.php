<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'hamster_description' => 'Affiche dans un calendrier les données issues d\'un rapport XML du logiciel Hamster',
	'hamster_nom' => 'Visualisation de Hamster (compteur de temps)',
	'hamster_slogan' => '',
);

?>